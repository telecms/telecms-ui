<div align="center">
  <a href='https://telecms-ui.com' target='_blank'>
    <img src="./docs/images/logo.png" alt="logo" width="200"  />
  </a>
</div>
<div align="center">
  <h1>Service Exchange</h1>
</div>


Service Exchange is a front-end low-code framework. Through Service Exchange, you can easily encapsulate any front-end UI components into low-code component libraries to build your own low-code UI development platform, making front-end development as tight as Service Exchange("mortise and tenon" in Chinese).


## Why Service Exchange?

### Reactive rendering low-code framework

Service Exchange chooses a reactive rendering solution that is easy to understand and has excellent performance, making Service Exchange intuitive and quick to start.

### Powerful low-code GUI editor

Service Exchange has a built-in GUI editor, which almost includes all the capabilities that a complete low-code editor should have.

### Extremely Extensible

Both the UI component library itself and the low-code editor support custom extensions. Developers can register various components to meet the needs of application and continue to use the existing visual design system.

### Type Safety

You are in type safety both when developing Service Exchange components and when using the Service Exchange editor. Service Exchange heavily uses Typescript and JSON schema for a great type system.

For more details, read [Service Exchange: A truly extensible low-code UI framework](./docs/en/what-is-Service Exchange.md).

## Tutorial

Service Exchange users are divided into two roles, one is developer and the other is user.

The responsibilities of developers are similar to those of common front-end developers. They are responsible for developing UI components and encapsulating common UI components to Service Exchange components. Developers need to write code to implement the logic of components.

The user's responsibility is to use the Service Exchange components encapsulated by developers to build front-end applications in the Service Exchange low-code editor. Users do not need front-end knowledge and programming skills. They can finish building the application through UI interaction only.

We have prepared two tutorials for user and developer. The user only needs to read the user's tutorial, while the developer must read both.

- [User's Tutorial](./docs/en/user.md)
- [Developer's Tutorial](./docs/en/developer.md)

## local development

### Start

```sh
yarn
cd packages/editor
yarn dev
```

### Test

```shell
yarn test:ci
```

### Build

```shell
yarn
```

> When you run the runtime or editor locally, if you modify the code of other packages, you must rebuild the modified package, otherwise, the runtime and editor will still run the old code.

## License

Apache-2.0
